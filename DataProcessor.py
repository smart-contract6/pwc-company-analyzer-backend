import os
import json

# def get_json_file(file_name):
#     folder_path = 'output_json'
#     file_path = os.path.join(folder_path, file_name)    
#     if os.path.isfile(file_path):
#         with open(file_path, 'r') as file:
#             data = json.load(file)
#         return data
#     else:
#         print(f"File '{file_name}' does not exist in the 'output_json' folder.")
#         return None
# allData = get_json_file("2 - Signed LRSI FS 2022.json")
# print(allData)

def generate_table_json(allData):
    # print(allData)
    data = allData['data']['tabular_data']
    key_value = allData['data']['key_value']
    text_data = allData['data']['text_data']
    generated_tables = {}
    for index, table_data in enumerate(data):
        table = {'header': [], 'data': []}        
        for key, value in table_data.items():
            row, col = value[0]
            cell_value = value[1]            
            while len(table['data']) <= row:
                table['data'].append([])
            while len(table['header']) <= col:
                table['header'].append(None)            
            if len(table['data'][row]) <= col:
                table['data'][row].extend([None] * (col - len(table['data'][row]) + 1))            
            if table['header'][col] is None:
                table['header'][col] = cell_value
            else:
                table['data'][row][col] = cell_value        
        generated_tables[f"table{index + 1}"] = table
    response = { 'tables': generated_tables , 'key_values': key_value, 'unstructured_data': text_data}
    final_response = json.dumps(response, indent=4)
    return final_response


def save_json_formated_response(allData, fileName):
    final_response = generate_table_json(allData)
    with open(fileName, 'w') as file:
        file.write(final_response)


# existing code...
# tabular_data = allData['data']['tabular_data']
# formated_data = generate_table_json(allData)
# formatted_json = json.dumps(formated_data, indent=4)
# print(formated_data)


