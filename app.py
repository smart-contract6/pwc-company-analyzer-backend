import os
from flask import Flask, request, jsonify, abort
from flask_cors import CORS
import json
from Utils import concatenate_json_files
from Pdf_utils import read_pdf, search_text_in_pdfs, find_text_in_pdf, process_file, read_files_from_folder
from Embedding import get_text_chunks, get_vector_store, delete_index_from_vector_store
from Gemini import answer_question
from FileReader import process_files
import asyncio
from dotenv import load_dotenv
from db.functions import get_all_templates, get_template_details, store_responses, get_answers
from db.model import db

load_dotenv()

app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("DATABASE_URL")
db.init_app(app)

#default route
@app.route('/', methods=['GET'])
def home():
    return "App is running..."

#all templates in db
@app.route('/get-all-templates', methods=['GET'])
def get_templates():
    try:
        template_list = get_all_templates()
        return jsonify(template_list)
    except Exception as e:
        abort(500, str(e))

#questions in the template
@app.route('/template', methods=['GET'])
def get_template():
    template_id = request.args.get('template_id', type=int)
    if template_id is None:
        abort(400, "Missing 'template_id' query parameter")

    try:
        template_data = get_template_details(template_id)
        if template_data is None:
            abort(404)

        return jsonify(template_data)
    except Exception as e:
        abort(500, str(e))

@app.route('/ask', methods=['POST'])
async def ask_question():
    ###### Step 1: Process the input files and save the output to a directory
    #print("File extraction started...")
    input_dir = os.getenv('FILE_INPUT_DIRICTORY')
    output_dir = os.getenv('FILE_TO_JSON_OUTPUT_DIRICTORY')
    url = os.getenv('FILE_EXTRACTOR_URL')
    # Get a list of all files in the directory
    files = os.listdir(output_dir)
    if not files:
        # If the list is empty, call the process_files function
        loop = asyncio.get_event_loop()
        await process_files(input_dir, output_dir, url)
    #print("File extraction finished...")
    ###### Step 2: Read the input files and concatenate the data
    data = request.get_json()
    responses = {}
    for segment_name, segment_data in data.items():
        segment_id = segment_data['category_id']
        segment_questions = [value for key, value in segment_data.items() if key != 'category_id']       
        text = read_files_from_folder(input_dir)
        directory = os.getenv('FILE_TO_JSON_OUTPUT_DIRICTORY')
        text = json.dumps(concatenate_json_files(directory))
        text_chunks = get_text_chunks(text)
        #print("Getting text chunk...")
        db = get_vector_store(text_chunks)
        #print("Embedding...")
        answers = []
        for question in segment_questions:
            response_text = await answer_question(question, text_chunks, segment_name) 

                      
            answers.append(response_text)
            
        delete_index_from_vector_store(db, 0)
        combined_answer = ', '.join(answers)
        responses[segment_name] = {
            'category_id': segment_id,
            'ans': combined_answer
        }
    return jsonify(responses)

#save answers
@app.route('/store-answer', methods=['POST'])
def store_response():
    data = request.json
    company_name = data.get('company_name')
    session_id = data.get('session_id')
    responses = data.get('responses')

    if not company_name or not session_id or not responses:
        return jsonify({'error': 'Missing required fields'}), 400

    result = store_responses(company_name, session_id, responses)
    if result is True:
        return jsonify({'message': 'Responses stored successfully'}), 200
    else:
        return jsonify({'error': result}), 500

#answers of a specific sessions
@app.route('/answers', methods=['GET'])
def get_answers_by_id():
    session_id = request.args.get('session_id')
    if not session_id:
        abort(400, "Missing 'session_id' query parameter")

    try:
        answers = get_answers(session_id)
        if not answers:
            return jsonify({'error': 'No answers found for the specified session ID.'}), 404

        return jsonify(answers), 200
    except Exception as e:
        abort(500, str(e))


@app.route('/upload', methods=['POST'])
def upload_files():
    # Get the uploaded files from the request
    uploaded_files = request.files.getlist('files')

    # Create a folder inside the input folder
    input_dir = os.getenv('FILE_INPUT_DIRICTORY')
    os.makedirs(input_dir, exist_ok=True)

    # Save the uploaded files to the created folder
    for file in uploaded_files:
        file.save(os.path.join(input_dir, file.filename))

    return jsonify({'success': 'file uploaded.'}), 200




if __name__ == "__main__":
    app.run(debug=True)
