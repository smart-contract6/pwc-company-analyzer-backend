from db.model import db, Template, Category, Question, Answer

def get_all_templates():
    try:
        templates = Template.query.all()
        template_list = [{'template_id': template.template_id, 'template_name': template.template_name} for template in templates]
        return template_list
    except Exception as e:
        raise e

def get_template_details(template_id):
    try:
        template = db.session.get(Template, template_id)
        if template is None:
            return None
        
        template_data = {}
        
        for category in template.categories:
            category_data = {'category_id': category.category_id}
            questions_dict = {}
            for i, question in enumerate(category.questions):
                questions_dict[f'q{i+1}'] = question.question_text
            category_data.update(questions_dict)
            template_data[category.category_name] = category_data
        
        return template_data
    except Exception as e:
        raise e

def store_responses(company_name, session_id, responses):

    try:
        for category_name, response_data in responses.items():
            category_id = response_data.get('category_id')
            answer_text = response_data.get('ans')
            if category_id and answer_text:
                answer = Answer(company_name=company_name, session_id=session_id, category_id=category_id, answer_text=answer_text)
                db.session.add(answer)
        db.session.commit()
        return True
    except Exception as e:
        db.session.rollback()
        return str(e)
    
def get_answers(session_id):
    answers = Answer.query.filter_by(session_id=session_id).all()
    
    company_answers = {}

    for answer in answers:
        company_name = answer.company_name
        category_id = answer.category_id
        ans = answer.answer_text
        answer_id = answer.answer_id

        answer_obj = {
            "answer_id": answer_id,
            "category_id": category_id,
            "ans": ans          
        }

        if company_name in company_answers:
            company_answers[company_name]["answers"].append(answer_obj)
        else:
            company_answers[company_name] = {
                "company_name": company_name,
                "answers": [answer_obj]
            }

    return list(company_answers.values())
