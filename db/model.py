from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Template(db.Model):
    template_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    template_name = db.Column(db.String(100), nullable=False)
    categories = db.relationship('Category', backref='template', lazy='joined')

class Category(db.Model):
    category_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    category_name = db.Column(db.String(100), nullable=False)
    template_id = db.Column(db.Integer, db.ForeignKey('template.template_id'), nullable=False)
    questions = db.relationship('Question', backref='category', lazy='joined')
    answers = db.relationship('Answer', backref='category', lazy='joined')

class Question(db.Model):
    question_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    question_text = db.Column(db.Text, nullable=False)
    category_id = db.Column(db.Integer, db.ForeignKey('category.category_id'), nullable=False)

class Answer(db.Model):
    answer_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    company_name = db.Column(db.String(100), nullable=False)
    session_id = db.Column(db.String(50), nullable=False)
    category_id = db.Column(db.Integer, db.ForeignKey('category.category_id'), nullable=False)
    answer_text = db.Column(db.Text, nullable=False)
