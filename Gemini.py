from langchain_google_genai import GoogleGenerativeAIEmbeddings, ChatGoogleGenerativeAI
import google.generativeai as genai
from langchain_community.vectorstores import FAISS
from langchain.chains.question_answering import load_qa_chain
from langchain.prompts import PromptTemplate
from dotenv import load_dotenv
import os

load_dotenv()

GEMINI_API_KEY = os.getenv('GOOGLE_API_KEY')
working_model = genai.GenerativeModel(os.getenv('AI_MODEL'))
genai.configure(api_key=GEMINI_API_KEY)


async def answer_question(user_question, text_chunks, segment_name):
    # print("text_chunks",text_chunks)
    embeddings = GoogleGenerativeAIEmbeddings(model= os.getenv('EMBEDDING_MODEL'))
    new_db = FAISS.load_local("faiss_index", embeddings)
    response = ""
    docs =  new_db.similarity_search(user_question)

    text = ""
    with open('Dictionary/synonymous.txt', 'r') as file:
        text = file.read()

    if docs:
        prompt = f"""
        Answer the Question depending on the given Context only.
                      
            Instructions:
            1. Answer the question in full setance based on the given context.
            2. Do not add any new information.
            3. Find the most relevant answer.
            4. Find the answer that is most similar to the question according to its given sector that is {segment_name}.
            7. If unable to find answer from the context return "".
            8. do not give ans if unable to find.

         Context:  {docs}
         Additional Instructions: {text}
         Question: {user_question} """

        print("Prompt", prompt)
        response = working_model.generate_content(prompt, generation_config={
                "max_output_tokens": 2048,
                "temperature": 0.1,
                "top_p": 1
            })
        print("response.text",response.text)    
        return response.text
    else:
        response = "No relevant context found."

        print("Answer finding...",response_data)
    return response


